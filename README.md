# README #

1/3スケールの富士通 FM-8風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- 富士通

## 発売時期
- FM-8 1981年

## 参考資料

- [富士通](https://www.fujitsu.com/jp/about/plus/museum/products/computer/personalcomputer/fm8.html)
- [Wikipedia](https://ja.wikipedia.org/wiki/FM-8)
- [IPSJ コンピュータ博物館](http://museum.ipsj.or.jp/computer/personal/0007.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-8/raw/84be7494d40012d892e2e0bb4981275b4bcc693f/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-8/raw/84be7494d40012d892e2e0bb4981275b4bcc693f/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-8/raw/84be7494d40012d892e2e0bb4981275b4bcc693f/ExampleImage.jpg)
